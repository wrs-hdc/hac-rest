// Test RESTful API for HDC2

//-------------------------------------------------
// Change these lines for your testing
var apiKey='api_key=a3zzn79c2escgdembcm2wep2'
var user = 'wrPaul'
var pass = 'cjj00flp'
// deviceId of my device
var uuid = '74f22231-487b-cb1c-16cc-a439fc5b55e6'
//-------------------------------------------------
var unirest = require('unirest')
var EndPointAddress = 'https://api.helixdevicecloud.com'
//var EndPointAddress = 'http://windriver.api.mashery.com/serverId'
/rest/deviceactions/v1

var da = '/rest/deviceactions/v1'
var ev = '/rest/events/v1'
var im = '/rest/issuesmanagement/v1'
var rp = '/rest/repository/v1'
var rm = '/rest/rulemanagement/v1'
var sm = '/rest/systemmanagement/v1'
var tm = '/rest/telemetry/v1'
var um = '/rest/usermanagement/v1'

defaultHeaders = 
    { 'Content-Type': 'application/json', 'Accept' : 'application/json' }
//-------------------------------------------------------------
 
/*---------------------------------------------------------------------
 Device Actions

https://api.helixdevicecloud.com/rest/deviceactions/v1
http://windriver.api.mashery.com/serverId/rest/deviceactions/v1

GET
    availableactions?$filter=filter&$offset=offset&$limit=limit
    availableactions/devices/deviceId?$offset=offset&$limit=limit
    actionstatus/{actionId}
    actionstatus?$filter=filter&$offset=offset&$limit=limit
    actionstatus/devices/deviceId?$offset=offset&$limit=limit

POST
    actions
    
*/
function apiGetTest(service, api, filter, offset, limit) {
    var request = EndPointAddress + service + api
    var separator = '?'
    if (filter || offset || limit) { separator = '&' }
    if (separator == '?') {
        request += separator + apiKey
    } else {
        request += filter + offset + limit + separator + apiKey
    }
    console.log(request)
    unirest.get(request)
    .auth(user, pass)
    .headers(defaultHeaders)
    .send()
    .end(function (response) {
        console.log(response.body)

    })
};



GET

https://api.helixdevicecloud.com/rest/

deviceactions/v1/
    availableactions?$filter=filter&$offset=offset&$limit=limit
    availableactions/devices/deviceId?$offset=offset&$limit=limit
    actionstatus/{actionId}
    actionstatus?$filter=filter&$offset=offset&$limit=limit
    actionstatus/devices/deviceId?$offset=offset&$limit=limit

apiGetTest(da, "availableactions")
apiGetTest(da, "availableactions")
apiGetTest(da, "actionstatus")
apiGetTest(da, "actionstatus")
apiGetTest(da, "actionstatus")

issuesmanagement/v1/
    issues/{id}
    issues?$filter=filter&offset=offset&$limit=limit
    issues/{issueID}/comments
    issues/{issueID}/comments/{commentId}

apiGetTest(im, "issues")
apiGetTest(im, "issues")
apiGetTest(im, "issues")
apiGetTest(im, "issues")

rulemanagement/v1/
    rules?$filter=filter&$offset=offset&$limit=limit
    rules/exported?filter=filter&$offset=offset&$limit=limit
    rules/{rule_id}
    metadata/actiontypes
    metadata/eventtypes?$filter=filter&$offset=offset&$limit=limit
    metadata/datatypes

apiGetTest(rm, "rules")
apiGetTest(rm, "rules/exported")
apiGetTest(rm, "rules")
apiGetTest(rm, "metadata/actiontypes")
apiGetTest(rm, "metadata/eventtypes")
apiGetTest(rm, "metadata/datatypes")

repository/v1/
    packages?$filter=filter&$offset=&$limit=limit
    deploy/{installId}
    deployments?$filter=filter&$limit=limit&$offset=offset

apiGetTest(rp, "packages")
apiGetTest(rp, "deploy")
apiGetTest(rp, "deployments")

events/v1/
    systemlogs/sources?$offset=offset&$limit=limit
    systemlogs/sourceID?$filter=filter&$offset=offset&$limit=limit

apiGetTest(ev, "systemlogs/sources")
apiGetTest(ev, "systemlogs/")

systemmanagement/v1/
    devices?$filter=filter&$offset=offset&$limit=limit&$orderby=orderby
    devices/{uuid}

apiGetTest(sm, "devices")
apiGetTest(sm, "devices/")

telemetry/v1/
    datasources?$filter=filter&offset=offset&$limit=limit
    datasources/devices/deviceId?$offset=offset&$limit=limit
    metricrecords?$filter=filter&$offset=offset&$limit=limit&$mode=mode
    metricrecords/devices/deviceId?$filter=filter&$offset=offset&$limit=limit&$mode=mode
    metricrecords/devices/deviceId/datasources/dataSourceName?$filter=filter&$offset=offset&$limit=limit&$mode=mode
    metricrecords/devices/deviceId/datasources/dataSourceName/metrics/metricName?$filter=filter&$offset=offset&$limit=limit&$mode=mode
    metricrecords/datasources/dataSourceName?$filter=filter&$offset=offset&$limit=limit&$mode=mode
    metricrecords/datasources/dataSourceName/metrics/metricName?$filter=&$offset=offset&$limit=limit&$mode=mode
    metricrecords/metrics/metricName?$filter=filter&$offset=offset&$limit=limit&$mode=mode
    metricrecordsnapshots?$filter=filter&$offset=offset&$limit=limit&$mode=mode
    metricrecordsnapshots/devices/deviceId?$filter=filter&$offset=offset&$limit=limit&$mode=mode
    metricrecordsnapshots/devices/deviceId/datasources/dataSourceName?$filter=filter&$offset=offset&$limit=limit&$mode=mode
    metricrecordsnapshots/devices/deviceId/datasources/dataSourceName/metrics/metricName
    metricrecordsnapshots/datasources/dataSourceName?$filter=filter&$offset=offset&$limit=limit&$mode=mode
    metricrecordsnapshots/datasources/dataSourceName/metrics/metricName?$filter=filter&$offset=offset&$limit=limit&$mode=mode
    metricrecordsnapshots/metrics/metricName?$filter=filter&$offset=offset&$limit=limit&$mode=mode

apiGetTest(tm, "datasources")
apiGetTest(tm, "datasources")
apiGetTest(tm, "metricrecords")
apiGetTest(tm, "metricrecords/devices")
apiGetTest(tm, "metricrecords/devices")
apiGetTest(tm, "metricrecords/devices")
apiGetTest(tm, "metricrecords/datasources")
apiGetTest(tm, "metricrecords/datasources")
apiGetTest(tm, "metricrecords/metrics")
apiGetTest(tm, "metricrecordsnapshots")
apiGetTest(tm, "metricrecordsnapshots/devices")
apiGetTest(tm, "metricrecordsnapshots/devices")
apiGetTest(tm, "metricrecordsnapshots/devices")
apiGetTest(tm, "metricrecordsnapshots/datasources")
apiGetTest(tm, "metricrecordsnapshots/datasources")
apiGetTest(tm, "metricrecordsnapshots/metrics")

usermanagement/v1/
    users
    users/{user_id}

apiGetTest(um, "users")
apiGetTest(um, "users/")
