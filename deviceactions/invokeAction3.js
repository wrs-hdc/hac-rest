// Test RESTful API for HDC2
//-------------------------------------------------
// Change these lines for your testing
var apiKey='api_key=a3zzn79c2escgdembcm2wep2';
var user = 'wrPaul'
var pass = 'cjj00flp'
// deviceId of my device
var uuid = '74f22231-487b-cb1c-16cc-a439fc5b55e6';
//-------------------------------------------------
var unirest = require('unirest');
var EndPointAddress = 'https://api.helixdevicecloud.com';
var da = '/rest/deviceactions/v1'
var defaultHeaders = 
    { 'Content-Type': 'application/json', 'Accept' : 'application/json' }
//-------------------------------------------------------------

var body = 
    { 
        'deviceId': [ uuid ],
        'serviceName': 'device_manager',
        'serviceVersion': '1.0',
        'actionName': 'led_on'
    }

function invokeAction() {
    request = EndPointAddress + da + '/actions' + '?' + apiKey
    console.log(request)
    unirest.post(request)
    .auth(user, pass)
    .headers(defaultHeaders)
    .send(body)
    .end(function (response) {
        console.log(
            response.body.data.items[0] //.actionName
        )
        var id = response.body.data.items[0].actionId
        console.log('id = ' + id)
        getActionStatus(id)
    })
}

function getActionStatus(id) {
    console.log('id2 = ' + id)
    var query = '{actionId: ' + id + '}'
    var request = EndPointAddress + da + '/actionstatus?$filter='
                + query + '&' + apiKey
    console.log(request)
    unirest.get(request)
    .auth(user, pass)
    .headers(defaultHeaders)
    .send()
    .end(function (response) {
        console.log(
            response.body.data.items[0].actionStatus
        )
    })
}

//-------------------------------------------------------------

invokeAction()
