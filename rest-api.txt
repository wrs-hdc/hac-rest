 
/---------------------------------------------------------------------/
Device Actions

http://windriver.api.mashery.com/serverId/rest/deviceactions/v1
https://api.helixdevicecloud.com/rest

GET
    availableactions?$filter=filter&$offset=offset&$limit=limit
    availableactions/devices/deviceId?$offset=offset&$limit=limit
    actionstatus/{actionId}
    actionstatus?$filter=filter&$offset=offset&$limit=limit
    actionstatus/devices/deviceId?$offset=offset&$limit=limit

POST
    actions
    

/---------------------------------------------------------------------/
Issues Management

https://api.helixdevicecloud.com/rest/
http://windriver.api.mashery.com/serverId/rest/issuesmanagement/v1
    
GET
    issues/{id}
    issues?$filter=filter&offset=offset&$limit=limit
    issues/{issueID}/comments
    issues/{issueID}/comments/{commentId}

POST
    issues
    issues/{issueID}/comments

PUT
    issues/{id}

DELETE
    issues/{id}
    issues?$filter={filter}


/---------------------------------------------------------------------/
Rule Management

https://api.helixdevicecloud.com/rest/rulemanagement/v1/

GET
    rules?$filter=filter&$offset=offset&$limit=limit
    rules/exported?filter=filter&$offset=offset&$limit=limit
    rules/{rule_id}
    metadata/actiontypes
    metadata/eventtypes?$filter=filter&$offset=offset&$limit=limit
    metadata/datatypes

POST
    rules
    rules/imported

PUT
    rules/{rule_id}

DELETE
    rules/{rule_id}

/---------------------------------------------------------------------/
Repository Management

https://api.helixdevicecloud.com/rest/repository/v1/

GET
    packages?$filter=filter&$offset=&$limit=limit
    deploy/{installId}
    deployments?$filter=filter&$limit=limit&$offset=offset

POST
    packages
    deploy
    deployments

DELETE
    packages/{productId}?$filter={filter}


/---------------------------------------------------------------------/
Events

https://api.helixdevicecloud.com/rest/rest/events/v1/

GET
    systemlogs/sources?$offset=offset&$limit=limit
    systemlogs/sourceID?$filter=filter&$offset=offset&$limit=limit


/---------------------------------------------------------------------/
System Management

GET
    devices?$filter=filter&$offset=offset&$limit=limit&$orderby=orderby
    devices/{uuid}

POST
    devices

DELETE
    devices/{uuid}


/---------------------------------------------------------------------/
Telemetry

https://api.helixdevicecloud.com/rest/telemetry/v1/

GET
    
    datasources?$filter=filter&offset=offset&$limit=limit

    datasources/devices/deviceId?$offset=offset&$limit=limit

    metricrecords?$filter=filter&$offset=offset&$limit=limit&$mode=mode

    metricrecords/devices/deviceId?$filter=filter&$offset=offset&$limit=limit&$mode=mode

    metricrecords/devices/deviceId/datasources/dataSourceName?$filter=filter&$offset=offset&$limit=limit&$mode=mode

    metricrecords/devices/deviceId/datasources/dataSourceName/metrics/metricName?$filter=filter&$offset=offset&$limit=limit&$mode=mode

    metricrecords/datasources/dataSourceName?$filter=filter&$offset=offset&$limit=limit&$mode=mode

    metricrecords/datasources/dataSourceName/metrics/metricName?$filter=&$offset=offset&$limit=limit&$mode=mode

    metricrecords/metrics/metricName?$filter=filter&$offset=offset&$limit=limit&$mode=mode

    metricrecordsnapshots?$filter=filter&$offset=offset&$limit=limit&$mode=mode

    metricrecordsnapshots/devices/deviceId?$filter=filter&$offset=offset&$limit=limit&$mode=mode

    metricrecordsnapshots/devices/deviceId/datasources/dataSourceName?$filter=filter&$offset=offset&$limit=limit&$mode=mode

    metricrecordsnapshots/devices/deviceId/datasources/dataSourceName/metrics/metricName

    metricrecordsnapshots/datasources/dataSourceName?$filter=filter&$offset=offset&$limit=limit&$mode=mode

    metricrecordsnapshots/datasources/dataSourceName/metrics/metricName?$filter=filter&$offset=offset&$limit=limit&$mode=mode

    metricrecordsnapshots/metrics/metricName?$filter=filter&$offset=offset&$limit=limit&$mode=mode

/---------------------------------------------------------------------/
User Management

https://api.helixdevicecloud.com/rest/usermanagement/v1/

GET
    users
    users/{user_id}

POST
    users

PUT
    users/{user_id}

DELETE
    users/{user_id}



