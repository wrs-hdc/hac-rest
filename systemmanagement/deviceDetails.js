// Test RESTful API for HDC2
//-------------------------------------------------
// Change these lines for your testing
var apiKey='api_key=a3zzn79c2escgdembcm2wep2';
var user = 'wrPaul'
var pass = 'cjj00flp'
// deviceId of my device
var uuid = '74f22231-487b-cb1c-16cc-a439fc5b55e6';
//-------------------------------------------------
var unirest = require('unirest');
var EndPointAddress = 'https://api.helixdevicecloud.com';
var sm = '/rest/systemmanagement/v1'
defaultHeaders = 
    { 'Content-Type': 'application/json', 'Accept' : 'application/json' }
//-------------------------------------------------------------

function deviceDetails() {
    request = EndPointAddress + sm + '/devices/' + uuid + '?' + apiKey
    console.log(request)
    unirest.get(request)
    .auth(user, pass)
    .headers(defaultHeaders)
    .send()
    .end(function(response) {
        console.log(
            response.body['device-status'].lastUpdateStatus + '    ' +
            response.body['iot-properties'].device_address + '    ' +
            response.body.properties.hostname
        )
    })
};

//-------------------------------------------------------------

deviceDetails()
