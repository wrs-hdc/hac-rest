// Test RESTful API for HDC2
//-------------------------------------------------
// Change these lines for your testing
var apiKey='api_key=a3zzn79c2escgdembcm2wep2';
var user = 'wrPaul'
var pass = 'cjj00flp'
// deviceId of my device
var uuid = '74f22231-487b-cb1c-16cc-a439fc5b55e6';
//-------------------------------------------------
var unirest = require('unirest');
var EndPointAddress = 'https://api.helixdevicecloud.com';
var tm = '/rest/telemetry/v1'
defaultHeaders = 
    { 'Content-Type': 'application/json', 'Accept' : 'application/json' }
//-------------------------------------------------------------

var tm_query = '{deviceId:{"$in":[' + uuid + ']}}';

function getTelemetry() {
    request = EndPointAddress + tm + '/metricrecordsnapshots?$filter=' 
            + tm_query + '&' + apiKey
    console.log(request)
    unirest.get(request)
    .auth(user, pass)
    .headers(defaultHeaders)
    .send()
    .end(function(response) {
        for (i=0; i<response.body.data.currentItemCount; i++) {
        	console.log(
        	    response.body.data.items[i].timestamp,
        	    response.body.data.items[i].metricName,
        	    response.body.data.items[i].value
        	    )
    	}
    })
};

//-------------------------------------------------------------

getTelemetry()
